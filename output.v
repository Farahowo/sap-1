
module OUTPUT(bus_in,clk,lobar);
  input clk, lobar;
  input [7:0] bus_in;
  wire [7:0] out;

  D_FF d0(out[0], ,bus_in[0],clk, 1'b1,1'b1,lobar);
  D_FF d1(out[1], ,bus_in[1],clk, 1'b1,1'b1,lobar);
  D_FF d2(out[2], ,bus_in[2],clk, 1'b1,1'b1,lobar);
  D_FF d3(out[3], ,bus_in[3],clk, 1'b1,1'b1,lobar);
  D_FF d4(out[4], ,bus_in[4],clk, 1'b1,1'b1,lobar);
  D_FF d5(out[5], ,bus_in[5],clk, 1'b1,1'b1,lobar);
  D_FF d6(out[6], ,bus_in[6],clk, 1'b1,1'b1,lobar);
  D_FF d7(out[7], ,bus_in[7],clk, 1'b1,1'b1,lobar);
  
  always @(lobar)
    $display("From %m at %d, OUTPUT = %b",$time, out);
endmodule