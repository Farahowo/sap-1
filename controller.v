/*
instruct = coming from IR as opcode
clear (active low)
HLT = Halt (Active low): not defined in the book
*/
module CONTROLLER(CP,EP,LMBAR,CEBAR,LIBAR,EIBAR,LABAR,EA,SU,EU,LBBAR,LOBAR,instruct,clear,clk,HLT);
  input [7:4] instruct;
  input clear,clk;
  output CP, EP, LMBAR, CEBAR, LIBAR, EIBAR, LABAR,EA, SU, EU, LBBAR, LOBAR;
  wire [7:4] instruct_n; //inverted instruction bit
  wire [6:1] states; /// T1 to T6 holder
  wire LDA, ADD, SUB, OUT/*, HLT*/;
  output HLT; //Implementation of HLT
  
  //variable for determining state number
  integer i,T;
  
  //inverted instruction set
  not(instruct_n[4],instruct[4]);
  not(instruct_n[5],instruct[5]);
  not(instruct_n[6],instruct[6]);
  not(instruct_n[7],instruct[7]);
  
  and(clrcounter,clear,NOPBAR); //combining with var machine cycle
  RINGCOUNTER counter(states,clk,clrcounter);
  
  always @(states)
  begin
    T = 0;
    for(i=6;i>0;i=i-1)
    begin
      if(states[i])
        T = i;
    end
    $display("From %m at %d, Current state = %d", $time, T);
  end
  
  //instruction decoder
  and(LDA,instruct_n[7],instruct_n[6],instruct_n[5],instruct_n[4]);
  and(ADD,instruct_n[7],instruct_n[6],instruct_n[5],instruct[4]);
  and(SUB,instruct_n[7],instruct_n[6],instruct[5],instruct_n[4]);
  and(OUT,instruct[7],instruct[6],instruct[5],instruct_n[4]);
  nand(HLT,instruct[7],instruct[6],instruct[5],instruct[4]);
  
  /*initial
  begin
  instruct = 4'b0000;
  #5 instruct = 4'b0001;
  #5 instruct = 4'b0010;
  #5 instruct = 4'b1110;
  #5 instruct = 4'b1111;
  end*/
  
  //control matrix
  buf(CP,states[2]);
  
  buf(EP,states[1]);
  
  and(lm1,LDA,states[4]);
  and(lm2,ADD,states[4]);
  and(lm3,SUB,states[4]);
  or(lm,states[1],lm1,lm2,lm3);
  not(LMBAR,lm);
  
  and(ce1,LDA,states[5]);
  and(ce2,ADD,states[5]);
  and(ce3,SUB,states[5]);
  or(ce,states[3],ce1,ce2,ce3);
  not(CEBAR,ce);
  
  not(LIBAR,states[3]);
  
  and(ei1,LDA,states[4]);
  and(ei2,ADD,states[4]);
  and(ei3,SUB,states[4]);
  or(ei,ei1,ei2,ei3);
  not(EIBAR,ei);
  
  and(la1,LDA,states[5]);
  and(la2,ADD,states[6]);
  and(la3,SUB,states[6]);
  or(la,la1,la2,la3);
  not(LABAR,la);
  
  and(EA,OUT,states[4]);
  
  and(SU,SUB,states[6]);
  
  and(eu1,ADD,states[6]);
  and(eu2,SUB,states[6]);
  or(EU,eu1,eu2);
  
  and(lb1,ADD,states[5]);
  and(lb2,SUB,states[5]);
  or(lb,lb1,lb2);
  not(LBBAR,lb);
  
  nand(LOBAR,OUT,states[4]);
  
  //implementation of variable machine cycle
  not(nCP,CP);
  not(nEP,EP);
  not(nEA,EA);
  not(nSU,SU);
  not(nEU,EU);
  
  nand(NOPBAR,nCP,nEP,LMBAR,CEBAR,LIBAR,EIBAR,LABAR,nEA,nSU,nEU,LBBAR,LOBAR);
  //end of variable machine cycle implementation
  
  always @(*)
    $display("\nCP=%b,EP=%b,LMBAR=%b,CEBAR=%b,LIBAR=%b,EIBAR=%b,LABAR=%b,EA=%b,SU=%b,EU=%b,LBBAR=%b,LOBAR=%b\n",CP,EP,LMBAR,CEBAR,LIBAR,EIBAR,LABAR,EA,SU,EU,LBBAR,LOBAR);

  always @(OUT)
    $display("OOOOOUUUUUTTT = %b",OUT);
endmodule
/*
module stimulus;
  reg clk, clear, instruct;
  wire CP,EP,LMBAR,CEBAR,LIBAR,EIBAR,LABAR,EABAR,SU,EU,LBBAR,LOBAR;
  
  CONTROLLER c1(CP,EP,LMBAR,CEBAR,LIBAR,EIBAR,LABAR,EABAR,SU,EU,LBBAR,LOBAR,instruct,clear,clk);
  
  initial
  clk = 1'b0;
  
  initial
  forever #5 clk = ~clk;
  
  initial
  begin
  clear = 1'b0;
  #10 clear = 1'b1;
  instruct = 4'b0000;
  #10 instruct = 4'b0001;
  #10 instruct = 4'b0010;
  #10 instruct = 4'b1110;
  #10 instruct = 4'b1111;
  #60 $stop;
  end
endmodule
*/