//positive edge triggered D flip flop
//clear, preset and enable are active low
module D_FF(q,qbar,d_in,clk,clear,preset,enable);
  output q, qbar;
  input d_in, clk, clear, preset, enable;
  wire w1, w2, w3, w4, d;
  
  nand n1(w1,w4,w2,preset);
  nand n2(w2,w1,clk,clear);
  nand n3(w3,w2,clk,w4);
  nand n4(w4,w3,d,clear);
  nand n5(q,w2,qbar,preset);
  nand n6(qbar,w3,q,clear);
  
  //enable implementation using MUX
  bufif0 b1(d,d_in,enable);
  bufif1 b2(d,q,enable);
endmodule
/*
module testbench;
  reg D, CLK, CLEAR, PRESET, ENABLE;
  wire Q, QBAR;
  
  D_FF f1(Q,QBAR,D,CLK,CLEAR,PRESET,ENABLE);
  
  initial
  CLK = 1'b0;
  
  initial
  forever
  #5 CLK = ~CLK;
  
  initial
  begin
    CLEAR = 1'b0; PRESET = 1'b1; ENABLE = 1'b0;
    #6 CLEAR = 1'b1;
    #5 D = 1'b1;
    #7 D = 1'b0;
    #13 ENABLE = 1'b1; D = 1'b1;
    #12 D = 1'b0;
    #20 $stop;
  end
endmodule
*/