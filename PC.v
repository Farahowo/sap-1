module PC(PCTBUS, clockbar, clearbar, cp, ep);
  input clockbar, clearbar, cp, ep;
  output [3:0] PCTBUS;
  wire [3:0] q;
  
  JK jk1(q[0], ,cp,cp,clockbar,clearbar);
  JK jk2(q[1], ,cp,cp,q[0],clearbar);
  JK jk3(q[2], ,cp,cp,q[1],clearbar);
  JK jk4(q[3], ,cp,cp,q[2],clearbar);  
  
  bufif1 buf1(PCTBUS[0],q[0],ep);
  bufif1 buf2(PCTBUS[1],q[1],ep);
  bufif1 buf3(PCTBUS[2],q[2],ep);
  bufif1 buf4(PCTBUS[3],q[3],ep);
  
  always @(q)
   $display("From %m at %d, Current address = %b", $time, q);
endmodule
/*
module stimulus;
  
  reg clockbar, clearbar, cp, ep;
  wire [3:0] PCTBUS;
  
  PC pc(PCTBUS, clockbar, clearbar, cp, ep);
  
  initial
    clockbar = 1'b0;
  
  initial
    forever #5 clockbar = ~clockbar;
  
  //the next two blocks are either or  
  initial
    begin
      clearbar = 1'b0;
      #5 clearbar = 1'b1; cp = 1'b1; ep = 1'b1;
      #160 clearbar = 1'b0;
      #20 $stop;
    end 
    
    initial
    begin
      clearbar = 1'b0;
      #5 clearbar = 1'b1; cp = 1'b1; ep = 1'b1;
      #20 cp = 1'b0;
      #20 cp = 1'b1;
      #160 $stop;
    end  
  
endmodule
*/