//reg_b comes from register b
module ADD_SUB (astbus, reg_b, actas, su, eu);
  input [7:0] actas, reg_b;
  input su, eu;
  output [7:0] astbus;
  wire [7:0] sum, input_b;
  
  xor x1(input_b[0], reg_b[0], su);
  xor x2(input_b[1], reg_b[1], su);
  xor x3(input_b[2], reg_b[2], su);
  xor x4(input_b[3], reg_b[3], su);
  xor x5(input_b[4], reg_b[4], su);
  xor x6(input_b[5], reg_b[5], su);
  xor x7(input_b[6], reg_b[6], su);
  xor x8(input_b[7], reg_b[7], su);
    
  fa f1(sum[0], c0, actas[0], input_b[0], su);
  fa f2(sum[1], c1, actas[1], input_b[1], c0);
  fa f3(sum[2], c2, actas[2], input_b[2], c1);
  fa f4(sum[3], c3, actas[3], input_b[3], c2);
  fa f5(sum[4], c4, actas[4], input_b[4], c3);
  fa f6(sum[5], c5, actas[5], input_b[5], c4);
  fa f7(sum[6], c6, actas[6], input_b[6], c5);
  fa f8(sum[7], c7, actas[7], input_b[7], c6);
  
  bufif1 buf1(astbus[0], sum[0], eu);
  bufif1 buf2(astbus[1], sum[1], eu);
  bufif1 buf3(astbus[2], sum[2], eu);
  bufif1 buf4(astbus[3], sum[3], eu);
  bufif1 buf5(astbus[4], sum[4], eu);
  bufif1 buf6(astbus[5], sum[5], eu);
  bufif1 buf7(astbus[6], sum[6], eu);
  bufif1 buf8(astbus[7], sum[7], eu);
  
  always @(eu)
    $display("From %m at %d, SUM = %b",$time,sum);
endmodule
/*
module stimulus;
  reg [7:0] ACTAS, REG_B;
  reg SU, EU;
  wire [7:0] ASTBUS;

  ADD_SUB as1(ASTBUS, REG_B, ACTAS, SU, EU);
  
  initial
  begin
    ACTAS = 8'B00111010; REG_B = 8'B00001000; EU= 1'B1; SU = 1'B0;
    //#5 SU = 1'B1;
    #10 EU = 1'B1;
    #20 $stop;
  end
endmodule
*/