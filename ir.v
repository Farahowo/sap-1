//li = Load IR (active low), ei = Enable BUS (Active low), clear (active high)
module IR(ctrl_out,bus_out,bus_in,li,clk,clear,ei);
  input li,clk,clear,ei;
  input [7:0] bus_in;
  output [3:0] ctrl_out;
  output [3:0] bus_out;
  wire [3:0] out;
  
  not(clr_n,clear); //making it active high
  //BUS Section
  D_FF f0(out[0],,bus_in[0],clk,clr_n,1'b1,li);
  D_FF f1(out[1],,bus_in[1],clk,clr_n,1'b1,li);
  D_FF f2(out[2],,bus_in[2],clk,clr_n,1'b1,li);
  D_FF f3(out[3],,bus_in[3],clk,clr_n,1'b1,li);
  //Controller sequencer
  D_FF f4(ctrl_out[0],,bus_in[4],clk,clr_n,1'b1,li);
  D_FF f5(ctrl_out[1],,bus_in[5],clk,clr_n,1'b1,li);
  D_FF f6(ctrl_out[2],,bus_in[6],clk,clr_n,1'b1,li);
  D_FF f7(ctrl_out[3],,bus_in[7],clk,clr_n,1'b1,li);
  
  //BUS out through three state buffer
  bufif0 b0(bus_out[0],out[0],ei);
  bufif0 b1(bus_out[1],out[1],ei);
  bufif0 b2(bus_out[2],out[2],ei);
  bufif0 b3(bus_out[3],out[3],ei);
  
  always @(ctrl_out)
    $display("From %m at %d, Instruction = %b", $time, ctrl_out);
  
  always @(out)
    $display("From %m at %d, BUS Out = %b",$time, out);
endmodule

/*module stimulus;
  reg [7:0] BUS_IN;
  reg CLK, LI, CLEAR, EI;
  wire [3:0] CTRL_OUT;
  wire [3:0] BUS_OUT;
  
  IR i1(CTRL_OUT,BUS_OUT,BUS_IN,LI,CLK,CLEAR,EI);
  
  initial
  CLK = 1'b0;
  
  initial
  forever #5 CLK = ~CLK;
  
  initial
  begin
    BUS_IN = 8'B00111010; CLEAR = 1'B1; LI= 1'B1; EI = 1'B1;
    #5 CLEAR = 1'B0;
    #10 LI = 1'B0;
    #15 EI = 1'B0;
    #20 $stop;
  end
endmodule*/