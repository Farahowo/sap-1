module mux_2_to_1(out,in1,s,in0);
  input in1, s, in0;
  output out;
  
  bufif1 buf1(out,in1,s);
  bufif0 buf2(out,in0,s);
  
endmodule