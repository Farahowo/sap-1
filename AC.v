//labar active low
module AC (actbus, actas, bustac, labar, clock, ea);
  output [7:0] actbus, actas;
  input [7:0] bustac;
  input labar, clock, ea;
  wire [7:0] q;
  
//bus to d_ff. since no clear or preset needed (which are active low), they are set to 1 by default
  D_FF d1(q[0], ,bustac[0],clock, 1'b1,1'b1,labar);
  D_FF d2(q[1], ,bustac[1],clock, 1'b1,1'b1,labar);
  D_FF d3(q[2], ,bustac[2],clock, 1'b1,1'b1,labar);
  D_FF d4(q[3], ,bustac[3],clock, 1'b1,1'b1,labar);
  D_FF d5(q[4], ,bustac[4],clock, 1'b1,1'b1,labar);
  D_FF d6(q[5], ,bustac[5],clock, 1'b1,1'b1,labar);
  D_FF d7(q[6], ,bustac[6],clock, 1'b1,1'b1,labar);
  D_FF d8(q[7], ,bustac[7],clock, 1'b1,1'b1,labar);

//d_ff to bus through bufif1  
  bufif1 buf1(actbus[0],q[0],ea);
  bufif1 buf2(actbus[1],q[1],ea);
  bufif1 buf3(actbus[2],q[2],ea);
  bufif1 buf4(actbus[3],q[3],ea);
  bufif1 buf5(actbus[4],q[4],ea);
  bufif1 buf6(actbus[5],q[5],ea);
  bufif1 buf7(actbus[6],q[6],ea);
  bufif1 buf8(actbus[7],q[7],ea);  
    
endmodule 

/*module stimulus;
  reg [7:0] BUSTAC;
  reg LABAR, CLOCK, EA;
  wire [7:0] ACTBUS, ACTAS;
  
  AC ac1(ACTBUS, ACTAS, BUSTAC, LABAR, CLOCK, EA);
  
  initial
  CLOCK = 1'b0;
  
  initial
  forever #5 CLOCK = ~CLOCK;
  
  initial
  begin
    BUSTAC = 8'B00111010; EA= 1'B0; LABAR = 1'B1;
    #10 LABAR = 1'B0;
    #15 EA = 1'B1;
    #20 $stop;
  end
endmodule*/